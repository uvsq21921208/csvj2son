package fr.uvsq21921208.csvjson;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;
import org.junit.Test;

public class JsonToCsvTest {
	
	 
	
	/**
	 * Test if the returned pointer to json file is not null.
	 **/
	@Test
	public void readJsonFileTest() {
		String filePath = "C://Users//Mouttie//Desktop//samples//example1.json";
	    String jsonFile = JsonToCsv.readJsonFile(filePath);
	    assertNotNull(jsonFile);
		
	}
	
	@Test
	/**
	 * Test if the ini parser is returning a valid pointer to the file. 
	 */
	public void parseConfigIniNotNullTest() {
		List < String > listOfAttributes = JsonToCsv.parseConfigIniFileAttributes();
		assertNotNull(listOfAttributes);
	}
	
	@Test
	/**
	 * Test if the ini parser is returning the right values for each section.
	 */
	public void parseConfigIniCorrectParsingTest() {
		List<String> listOfAttributes = new ArrayList<String>();
	    String path = "C://Users//Mouttie//Desktop//CSV2JSON//files//config.ini";
	    Wini ini;
	    try {
	      ini = new Wini(new File(path));
	      String section = ini.get("AttributesJSON", "att");
	      listOfAttributes = Arrays.asList(section.split("\\s*,\\s*"));

	    } catch (InvalidFileFormatException e) {

	      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
		  String elementOne = listOfAttributes.get(0);
		  String elementTwo = listOfAttributes.get(1);
		  assertEquals(elementOne,"fruit");
		  assertEquals(elementTwo,"size");
	}
	/**
	 * Test if the returned string is a valid csv string.
	 */
	@Test
	public void writeJsonArrayAsCommaSeperatedStringTest() {
		String jsonString ="{\r\n" + 
				"    \"fruit\": \"Apple\",\r\n" + 
				"    \"size\": \"Large\",\r\n" + 
				"    \"color\": \"Red\"\r\n" + 
				"}";
		Map<String, String>  flattenJson = FlattenJson.flatten(jsonString);
		
		String csvStringExpected = "size,color,fruit\n" + 
				"Large,Red,Apple\n";
		
		String csvStringReturned = JsonToCsv.writeJsonArrayAsCsv(flattenJson);
		System.out.println(csvStringExpected);
		System.out.println(csvStringReturned);
		
		assertEquals(csvStringExpected,csvStringReturned);
	}
	
	
}
