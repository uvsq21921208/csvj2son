package fr.uvsq21921208.csvjson;

import static org.junit.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.junit.Test;

public class FlattenJsonTest {

	@Test
	public void flattenTest() {
		String nestedJson = "{ \"a\":\r\n" + 
				"  { \"b\": 1,\r\n" + 
				"    \"c\": null,\r\n" + 
				"    \"d\": [false, true]\r\n" + 
				"  },\r\n" + 
				"  \"e\": \"f\",\r\n" + 
				"  \"g\": 2.3\r\n" + 
				"}";
		
		
		LinkedHashMap<String, String> returnedFlatJson = FlattenJson.flatten(nestedJson);
		LinkedHashMap<String, String> expectedFlatjson = new LinkedHashMap<String,String>();
		
		expectedFlatjson.put("a.b", "1");
		expectedFlatjson.put("a.c", "null");
		expectedFlatjson.put("a.d[0]", "false");
		expectedFlatjson.put("a.d[1]", "true");
		expectedFlatjson.put("e", "f");
		expectedFlatjson.put("g", "2.3");
		assertTrue(expectedFlatjson.equals(returnedFlatJson));
		  
	}
}
