package fr.uvsq21921208.csvjson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvMalformedLineException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;




public class CsvToJsonTest {
	//create csvToJson objects
	CsvToJson csvj1=new CsvToJson("C://Users//Mouttie//Desktop//CSV2JSON//files//CSV_sample.csv");
	CsvToJson csvj2=new CsvToJson("C://Users//Mouttie//Desktop//CSV2JSON//files//access-code.csv");
	CsvToJson ncsvj3=new CsvToJson("C://Users//Mouttie//Desktop//CSV2JSON//files//M1_MAN_Algo_2017.pdf");
	CsvToJson ncsvj4=new CsvToJson("C://Users//Mouttie//Desktop//CSV2JSON//files//my_file.csv");
	CsvToJson csvj5=new CsvToJson("C://Users//Mouttie//Desktop//CSV2JSON//files//test.csv");
	
	/**
	 * test fullName of  csv file  
	 **/
	@Test
	public void  getFileNameTest() {
		assertEquals(csvj1.getCsvPath(),"C://Users//Mouttie//Desktop//CSV2JSON//files//CSV_sample.csv");
		assertEquals(csvj2.getCsvPath(),"C://Users//Mouttie//Desktop//CSV2JSON//files//access-code.csv");	
	}
	/**
	 * test the converted fullName of json file
	 **/
	@Test
	public void getJsonFileName() {
		//convert csv file  name 1 to json name 1
		assertEquals(csvj1.getJsonPath(),"C://Users//Mouttie//Desktop//CSV2JSON//files//CSV_sample.json");
		//convert csv file name 2 to json name 2
		assertEquals(csvj2.getJsonPath(),"C://Users//Mouttie//Desktop//CSV2JSON//files//access-code.json");
		
	}
	/**
	 * test headers of csv file
	 * @throws FileNotFoundException File not found exception.
	 * @throws CsvMalformedLineException  Not valid csv exception.
	 * @throws CsvException all exceptions for opencsv.
	 * @throws IOException an In/Out exception.
	 **/
	@Test
	public void getHeadresCsvTest() throws FileNotFoundException,
    CsvMalformedLineException, CsvException, IOException {
		//headers file 1
		assertEquals(Arrays.toString(csvj1.getHeadersCsv()),"[EMAIL, LASTNAME, FIRSTNAME, SMS]");
		//headers file 2
		assertEquals(Arrays.toString(csvj2.getHeadersCsv()),"[Identifier, First name, Last name]");
		
	}
	/**
	 * test si le fichier n'existe pas
	 * 
	 * @throws FileNotFoundException
	 */
	@Test(expected = FileNotFoundException.class)
	public void findCsvFile() throws FileNotFoundException {
		 CSVReader csv = new CSVReader(new FileReader(ncsvj4.getCsvPath()));
		
	}
	/**
	 * test si le fichier n'est pas de type csv
	 * @throws FileNotFoundException File not found exception.
	 * @throws CsvMalformedLineException  Not valid csv exception.
	 * @throws CsvException all exceptions for opencsv.
	 * @throws IOException an In/Out exception.
	 **/
	@Test(expected = CsvMalformedLineException.class)
	public void readCsvFiel() throws FileNotFoundException,
    CsvMalformedLineException, CsvException, IOException {
		 CSVReader csv = new CSVReader(new FileReader(ncsvj3.getCsvPath()));
		 csv.readAll();
		 csv.close();
		
	}
	/**
	 * method to compare the content of two files
	 * 
	 * @param fileName1 path of file 1
	 * @param fileName2 path of file 2
	 * @return true:if the two files have the same content else false
	 **/
	public boolean compareTwoFiles(String fileName1, String fileName2){
	      Path p1 = Paths.get(fileName1);
	      Path p2 = Paths.get(fileName2);

	try{
	        List<String> listF1 = Files.readAllLines(p1);
	    List<String> listF2 = Files.readAllLines(p2);
	    return listF1.containsAll(listF2);

	        }catch(IOException ie) {
	            ie.getMessage();
	        }
	return false;

	    }

	
	/**
	 * test the content of the expected and returned json files.
	 * @throws FileNotFoundException File not found exception.
	 * @throws CsvMalformedLineException  Not valid csv exception.
	 * @throws CsvException all exceptions for opencsv.
	 * @throws IOException an In/Out exception.
	 */
	@Test
	public void writeJsonFile() throws FileNotFoundException,
    CsvMalformedLineException, CsvException, IOException {
		csvj5.convertToJson();
		String jsonFileNameReturned=csvj5.getJsonPath();
		String jsonFileNameExpected="C://Users//Mouttie//Desktop//CSV2JSON//files//jsonFile_expected.json";
		String jsonExpected="[\n"+
		                    "  {\n"+
		                    "    \""+"Nom"+"\""+":"+"\"Dupont"+""+"\""+",\n"+
		                    "    \""+"Prenom"+"\""+":"+"\"Jules"+""+"\""+",\n"+
		                    "    \""+"Localisation"+"\""+":"+"\"Paris"+""+"\""+"\n"+
		                    "  },\n"+
		                    "  {\n"+
		                    "    \""+"Nom"+"\""+":"+"\"Martin"+""+"\""+",\n"+
		                    "    \""+"Prenom"+"\""+":"+"\"Melissa"+""+"\""+",\n"+
		                    "    \""+"Localisation"+"\""+":"+"\"London"+""+"\""+"\n"+
		                    "  }\n"+	  
		                    "]";
		 BufferedWriter  jsonFileExpected = new BufferedWriter(new FileWriter(new File(jsonFileNameExpected)));		
		 jsonFileExpected.write(jsonExpected);
		 jsonFileExpected.close();
		 
		 assertTrue(compareTwoFiles(jsonFileNameReturned, jsonFileNameExpected));
				
	}
}
