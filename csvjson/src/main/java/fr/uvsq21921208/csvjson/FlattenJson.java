package fr.uvsq21921208.csvjson;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ValueNode;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A helper class that flats a nested json.
 * @author Mouttie Djekhaba and Sarra Belmahdi.
 *
 */

public final class FlattenJson {
  /**
   * private constructor.
   */
  private FlattenJson() {
  }

  /**
  * Converts a nested json into a flat one represented as a hash map.
  * @param json a valid json strig
  * @return a hash map representation of the flatten json.
  */
  public static LinkedHashMap<String, String> flatten(final String json) {
    LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
    try {
      addKeys("", new ObjectMapper().readTree(json), map);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return map;
  }

  /**
   * helper method to add keys to map.
   * @param currentPath current path from root to this node.
   * @param jsonNode a json node.
   * @param map map that stores key with the value of json node.
   */
  private static void addKeys(final String currentPath, final JsonNode jsonNode,
      final LinkedHashMap<String, String> map) {
    if (jsonNode.isObject()) {
      ObjectNode objectNode = (ObjectNode) jsonNode;
      Iterator<Map.Entry<String, JsonNode>> iter = objectNode.fields();
      String pathPrefix = currentPath.isEmpty() ? "" : currentPath + ".";

      while (iter.hasNext()) {
        Map.Entry<String, JsonNode> entry = iter.next();
        addKeys(pathPrefix + entry.getKey(), entry.getValue(), map);
      }
    } else if (jsonNode.isArray()) {
      ArrayNode arrayNode = (ArrayNode) jsonNode;
      for (int i = 0; i < arrayNode.size(); i++) {
        addKeys(currentPath + "[" + i + "]", arrayNode.get(i), map);
      }
    } else if (jsonNode.isValueNode()) {
      ValueNode valueNode = (ValueNode) jsonNode;
      map.put(currentPath, valueNode.asText());
    }
  }
}
