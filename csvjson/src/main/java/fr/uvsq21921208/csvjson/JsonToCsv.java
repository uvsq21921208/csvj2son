/**
 * csvjson package that containts needed classes.
 */

package fr.uvsq21921208.csvjson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;
import org.json.JSONArray;
import org.json.JSONObject;

/**
* JsonToCsv class provides a tool to convert a json file to a standard csv file.
* @author Mouttie Djekhaba and Sarra Belmahdi.
*
*/

public final class JsonToCsv {
  /**
  * create Config parser file.
  */
  private static ConfigParser cp = new ConfigParser();
  /**
  * private constructor.
  */

  private JsonToCsv() {
  }

  /**
  * Read json file from the given path.
  * @param filePath = path to json file.
  * @return a string that contains the whole parsed json file.
  */
  static String readJsonFile(final String filePath) {

    String jsonFile = null;
    try {
      jsonFile = new String(Files.readAllBytes(Paths.get(filePath)), "utf-8");
    } catch (IOException e) {
      System.out.println("File not found");
    }
    return jsonFile;
  }

  /**
  * return number of lines in json file.
  * @param filePath Path to json file.
  * @return lines number of lines.
  */
  static int getNumberOfLines(final String filePath) {
    int lines = 0;
    try {
      lines = Files.readAllLines(Paths.get(filePath)).size();
    } catch (IOException e) {
      System.out.println("File not found");
    }
    return lines;
  }

  /**
  * a helper methode to parse windows ini config files.
  * @return The list of parsed Ini file sections.
  */

  public static List<String> parseConfigIniFileOperations() {
    if (!cp.configFileExists()) {
      cp.createConfigIni();
    }
    List<String> operations = new ArrayList<String>();
    String path = "config.ini";
    Wini ini;
    try {
      ini = new Wini(new File(path));
      for (String name : ini.get("OperationsJSON").keySet()) {
        String operation = name + " = " + ini.get("OperationsJSON", name);
        operations.add(operation);
      }

    } catch (InvalidFileFormatException e) {

      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return operations;
  }

  /**
  * Return list of operations to be executed.
  * @return list of Attributes that should be parsed.
  */

  public static List<String> parseConfigIniFileAttributes() {
    if (!cp.configFileExists()) {
      cp.createConfigIni();
    }
    List<String> listOfAttributes = new ArrayList<String>();
    String path = "config.ini";
    Wini ini;
    try {
      ini = new Wini(new File(path));
      String section = ini.get("AttributesJSON", "att");
      listOfAttributes = Arrays.asList(section.split("\\s*,\\s*"));

    } catch (InvalidFileFormatException e) {

      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return listOfAttributes;
  }

  /**
  * converts a flat json file (represented as a map with pair (key,value)) to a
  *  valid csv string.
  * @param flattenJson a valid flat json represented as a map(String,String).
  * @return a string representing the conversion result.
  */
  static String writeJsonArrayAsCsv(final Map<String, String> flattenJson) {
    JSONArray jsonArray = new JSONArray();
    jsonArray.put(flattenJson);
    String keysString = "";
    String valuesString = "";
    for (int i = 0; i < jsonArray.length(); i++) {
      JSONObject objects = jsonArray.getJSONObject(i);
      JSONArray key = objects.names();
      StringBuffer keyStringBuffer = new StringBuffer();
      StringBuffer valuesStringBuffer = new StringBuffer();
      for (int j = 0; j < key.length(); ++j) {
        String keys = key.getString(j);
        String value = objects.getString(keys);
        keyStringBuffer.append(keys);
        keyStringBuffer.append(",");
        valuesStringBuffer.append(value);
        valuesStringBuffer.append(",");
      }
      keysString = keyStringBuffer.toString();
      valuesString = valuesStringBuffer.toString();
      keysString = keysString.substring(0,
                                          keysString.length() - 1);
      valuesString = valuesString.substring(0,
                                              valuesString.length() - 1);
      keysString += "\n";
      valuesString += "\n";

    }
    return keysString + valuesString;


  }
  /**
   * Writes a valid csv string to a csv file.
   * @param csv valid csv string.
   */

  private static void writeToCsv(final String csv) {
    try {
      OutputStream outputStream = new FileOutputStream("output.csv");
      PrintWriter out = new PrintWriter(new OutputStreamWriter(
          outputStream, StandardCharsets.UTF_8), true);
      out.print(csv);
      out.close();

    } catch (FileNotFoundException e) {

      e.printStackTrace();
    }


  }
  /**
 * Convert a json file to a csv file.
 * @param filePath a valid path to json file.
 */

  public static void convertJsonToCsv(final String filePath) {
    boolean specificAtt = false;
    String output = readJsonFile(filePath);
    Map<String, String> flattenJson = FlattenJson.flatten(output);
    Map<String, String> flatJsonSpecificAtt = new HashMap<String, String>();
    List<String> listOfattributes = parseConfigIniFileAttributes();
    List<String> listOfOperations = parseConfigIniFileOperations();
    String csv = "";
    JSONObject job = new JSONObject(flattenJson);
    Set<String> nameSet = job.keySet();
    String name = nameSet.iterator().next();
    name = name.substring(0, name.indexOf("["));
    for (int k = 0; k < getNumberOfLines(filePath) - 2; k++) {
      // Handle attributes sections.
      if (listOfattributes.size() == 1 && listOfattributes.get(0).equals("*")) {
        csv = writeJsonArrayAsCsv(flattenJson);
      } else {
        for (int i = 0; i < listOfattributes.size(); i++) {
          String key = listOfattributes.get(i);
          flatJsonSpecificAtt.put(name + "[" + k + "]." + key,
              (String) flattenJson.get(name + "[" + k + "]." + key));
          specificAtt = true;
        }
        csv = writeJsonArrayAsCsv(flatJsonSpecificAtt);
      }
      //Handle operations section.
      for (int i = 0; i < listOfOperations.size(); i++) {
        String operationString = listOfOperations.get(i);
        int index = operationString.indexOf("=");
        String key = operationString.substring(0, index - 1);
        key = key.trim();
        operationString = operationString.substring(index,
                  operationString.length());
        operationString = operationString.replace("\\s*=\\s*", "");
        String operation = "";
        if (operationString.contains("*")) {
          operation = "*";
        }
        if (operationString.contains("+")) {
          operation = "+";
        }
        if (operationString.contains("/")) {
          operation = "/";
        }
        if (operationString.contains("-")) {
          operation = "-";
        }

        if (operation.equals("")) {
          System.out.print("Invalid operation, please check your config file.");
          System.exit(1);
        }
        String[] operandes = operationString.split("\\s*" + "\\" + operation
                                                               + "\\s*");
        String firstOperande = name + "[" + k + "]."
                                    + operandes[0].replaceAll("\\s*=\\s*", "");
        String secondOperande = name + "[" + k + "]." + operandes[1];
        double valueDouble = 0;
        double firstDouble = 0;
        double secondDouble = 0;
        String firstOpKey = flattenJson.get(firstOperande);

        try {
          firstDouble = Double.parseDouble(firstOpKey);
        } catch (NumberFormatException e) {
          System.out.println("Attribute " + firstOpKey
                       + " has no numerical value, "
              + "please check your config file!");
          System.exit(1);
        }
        String secondOpKey = flattenJson.get(secondOperande);
        try {
          secondDouble = Double.parseDouble(secondOpKey);
        } catch (NumberFormatException e) {
          System.out.println("Attribute " + secondOpKey
                       + " has no numerical value, "
              + "please check your config file!");
          System.exit(1);
        }

        switch (operation) {
          case "*":
            valueDouble = firstDouble * secondDouble;
            break;
          case "+":
            valueDouble = firstDouble + secondDouble;
            break;
          case "/":
            valueDouble = firstDouble / secondDouble;
            break;
          case "-":
            valueDouble = firstDouble - secondDouble;
            break;
          default:
            break;
        }
        if (specificAtt) {
          flatJsonSpecificAtt.put(name + "[" + k + "]."
                      + key, String.valueOf(valueDouble));
        } else {
          flattenJson.put(name + "[" + k + "]."
                      + key, String.valueOf(valueDouble));
        }
      }
      if (specificAtt) {
        csv = writeJsonArrayAsCsv(flatJsonSpecificAtt);
      } else {
        csv = writeJsonArrayAsCsv(flattenJson);
      }
    }
    writeToCsv(csv);
  }
}

