package fr.uvsq21921208.csvjson;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvValidationException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;

/**
 * A class that provides a method to convert any csv file to json file.
 *
 * @author Mouttie Djekhaba and Sarra Belmahdi.
 *
 */

public class CsvToJson {
  /**
  * csv file path.
  */
  private final String fileName;
  /**
    * create Config parser file.
    */
  private static ConfigParser cp = new ConfigParser();

  /**
  * Constructor, takes a file name as an argument.
  *
  * @param fileNameArg csv full path.
  */
  public CsvToJson(final String fileNameArg) {
    this.fileName = fileNameArg;
  }

  /**
  * getter for file path.
  *
  * @return file path.
  */
  public String getCsvPath() {
    return fileName;
  }

  /**
  * file name getter.
  *
  * @return json file name
  */
  public String getJsonPath() {
    // path where it will be creating the file with json extension
    String jsonFileName = getCsvPath().toString()
        .substring(0, getCsvPath().toString().lastIndexOf(".")) + ".json";
    return jsonFileName;
  }

  /**
  * returns csv headers plus added columns name.
  *
  * @return An array containing headers of csv file.
  * @throws IOException            an In/Out exception.
  * @throws CsvValidationException Not valid csv exception
  * @throws FileNotFoundException  File not found exception
  */
  public String[] getHeadersCsv() throws IOException,
      CsvValidationException, FileNotFoundException {
    InputStream inputStream = new FileInputStream(getCsvPath());
    Reader fileReader = new InputStreamReader(inputStream, "UTF-8");
    CSVReader csv = new CSVReader(fileReader);
    String[] headers = csv.readNext();
    csv.close();
    String[] newHeaders = getNewColumnsName();
    String[] allHeaders = Stream.concat(Arrays.stream(headers),
      Arrays.stream(newHeaders)).toArray(String[]::new);
    return allHeaders;
  }
  /**
   * a method that converts a csv to a string matrix.
   *
   * @param csvFileName csv file name.
   * @return a csv string represented as a matrix (Columns are headers, lines
   * are values).
   *@throws IOException           an In/Out exception
   * @throws FileNotFoundException File not found exception
   * @throws CsvException          all exceptions for opencsv
  */

  public String[][] csvToString(final String csvFileName) throws IOException,
      FileNotFoundException, CsvException {
    InputStream inputStream = new FileInputStream(csvFileName);
    Reader fileReader = new InputStreamReader(inputStream, "UTF-8");
    CSVReader csv = new CSVReader(fileReader);

    // lire tout le fichier
    List<String[]> lines = csv.readAll();
    //enlever headers
    lines.remove(0);
    //nb ligne
    int nbLines = lines.size();
    //nb colonne
    int nbColumns = lines.get(0).length;
    //matrice pour stocker data du fichier
    final String[][]data = new String[nbLines][nbColumns];
    int i = 0;
    for (String[]datas:lines) {
      if (datas != null && i < data.length) {
        for (int j = 0; j < nbColumns; j++) {
          data[i][j] = datas[j];
        }
      }
      i++;
    }
    csv.close();
    return data;
  }

  /**
  * a method that gets csv data + added columns data.
  *
  * @param csvFileName csv file name.
  * @return a csv string represented as a matrix (Columns are headers, lines
  * are values).
  * @throws IOException           an In/Out exception
  * @throws FileNotFoundException File not found exception
  * @throws CsvException          all exceptions for opencsv
  */


  public String[][] getAllData(final String csvFileName) throws IOException,
      FileNotFoundException, CsvException {
    String[][] dataCsv = csvToString(getCsvPath());
    //nb ligne
    int nbLines = dataCsv.length;
    //nb colonne
    int nbColumnsData = dataCsv[0].length;
    //get added columns
    List<String[]> addedColumns = treateOperations();
    //get size of added columns
    int nbAddedColumns = addedColumns.size();
    //nb colonne
    int nbColumns = nbColumnsData + nbAddedColumns;
    //matrice pour stocker data du fichier plus les colonnes ajoutées
    final String[][]data = new String[nbLines][nbColumns];
    for (int i = 0; i < data.length; i++) {
      for (int j = 0; j < nbColumnsData; j++) {
        data[i][j] = dataCsv[i][j];
      }
    }
    int j = 0;
    for (String[] datas : addedColumns) {
      for (int k = 0; k < nbLines; k++) {
        data[k][j + nbColumnsData] = datas[k];
      }
      j++;
    }
    return data;
  }

  /**
  * a helper methode to parse windows ini config files.
  *
  * @return The list of parsed Ini file sections.
  */
  public static List<String> parseConfigIniFileCsv() {
    if (!cp.configFileExists()) {
      cp.createConfigIni();
    }
    List<String> attributesCsv = new ArrayList<String>();
    String path = "config.ini";
    Wini ini;
    try {
      ini = new Wini(new File(path));
      String section = ini.get("AttributesCSV", "att");
      attributesCsv = new ArrayList<String>(Arrays.asList(section
        .split("\\s*,\\s*")));
    } catch (InvalidFileFormatException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return attributesCsv;
  }

  /**
  * get index of selected attributes and added attributes.
  *
  * @return array contains index of selected attributes.
  * @throws FileNotFoundException  File not found exception.
  * @throws IOException            an In/Out exception.
  * @throws CsvValidationException Not valid csv exception.
  */
  public int[] getIndexColumns() throws FileNotFoundException,
      IOException, CsvValidationException {
    //get headers
    String[] headers = getHeadersCsv();
    //get added columns
    String[] newColumns = getNewColumnsName();
    // get csv attributes from config.ini
    List<String> attributesCsv = parseConfigIniFileCsv();
    int nbColumns;
    int[] indexColumns;
    //if user select all  attributes
    if (attributesCsv.size() == 1 && attributesCsv.get(0).equals("*")) {
      nbColumns = headers.length;
      attributesCsv.clear();
      for (int i = 0; i < headers.length; i++) {
        attributesCsv.add(headers[i]);
      }
      // table of columns attributes index
      indexColumns = new int[nbColumns];
      int m = 0;
      // get columns dat attribut index
      for (int i = 0; i < headers.length; i++) {
        for (String column : attributesCsv) {
          if (column.equals(headers[i]) && m < indexColumns.length) {
            indexColumns[m] = i;
            m++;
          }
        }
      }
      return indexColumns;

    // user select some attributtes
    } else {

      //add nb selected columns
      nbColumns = attributesCsv.size();
      int nbColumnsAdd = newColumns.length;
      // table of columns attributes index
      indexColumns = new int[nbColumns + nbColumnsAdd];
      int m = 0;
      // get columns dat attribut index
      for (int i = 0; i < headers.length; i++) {
        for (String column : attributesCsv) {
          if (column.equals(headers[i]) && m < indexColumns.length) {
            indexColumns[m] = i;
            m++;
          }
        }
        for (String column : newColumns) {
          if (column.equals(headers[i]) && m < indexColumns.length) {
            indexColumns[m] = i;
            m++;
          }
        }
      }
      return indexColumns;
    }
  }
  /**
   * get newcolumns name.
   * @return table of new columns name.
   */

  public String[] getNewColumnsName() {
    List<String> operationsCsv = parseConfigIniFileOperations();
    List<String> newColumnName = new ArrayList<>();
    for (int i = 0; i < operationsCsv.size(); i++) {
      String operationString = operationsCsv.get(i);
      if (!operationString.contains("null")) {
        int index = operationString.indexOf("=");
        String key = operationString.substring(0, index - 1).trim();
        operationString = operationString.substring(index + 1,
          operationString.length()).trim();
        newColumnName.add(key);
      }
    }

    String[] newColumnNameA = newColumnName.toArray(
      new String[newColumnName.size()]);
    return newColumnNameA;
  }
  /**
   * get operation of added aperation.
   * @param operationString1 full line of added operatin.
   * @return operation.
   */

  public String getOperation(final String operationString1) {
    int index = operationString1.indexOf("=");
    String operationString = operationString1.substring(index + 1,
                  operationString1.length()).trim();
    String operation = "";
    if (operationString.contains("*")) {
      operation = "*";
    }
    if (operationString.contains("+")) {
      operation = "+";
    }
    if (operationString.contains("/")) {
      operation = "/";
    }
    if (operationString.contains("-")) {
      operation = "-";
    }

    if (operation.equals("")) {
      System.out.print("Invalid operation, please check your config file.");
    }
    return operation;
  }

  /**
   * methode to hadel an arithemitic operation.
   * @param operande1 first opeande.
   * @param operande2 second operande.
   * @param operation operation to handel.
   * @return restult of operation.
   */

  public String handelOperation(final double operande1,
      final double operande2, final String operation) {
    double resultat = 0.0;
    switch (operation) {
      case "*":
        resultat = operande1 * operande2;
        break;
      case "+":
        resultat = operande1 + operande2;
        break;
      case "/":
        resultat = operande1 / operande2;
        break;
      case "-":
        resultat = operande1 - operande2;
        break;
      default:
        break;
    } return Double.toString(resultat);
  }
  /**
   * method helps to handel config file added operations.
   * @return list of String arrays of all operations.
   * @throws FileNotFoundException  File not found exception
   * @throws IOException            an In/Out exception.
   * @throws CsvException           all exceptions for opencsv.
   */

  public  List<String[]> treateOperations() throws FileNotFoundException,
      IOException, CsvException {
    List<String> operationsCsv = parseConfigIniFileOperations();
    List<String[]> newColumnsOperations = new ArrayList<>();
    //get csv data
    String[][]data = csvToString(getCsvPath());
    //get nb lines in csv file
    int nblines = data.length;
    //Handle operations section.
    for (int i = 0; i < operationsCsv.size(); i++) {
      String operationString = operationsCsv.get(i);
      if (!operationString.contains("null")) {
        String operation = getOperation(operationString);
        int index = operationString.indexOf("=");
        //handel operandes
        operationString = operationString.substring(index,
           operationString.length());
        String[] operandes = operationString.split("\\s*" + "\\" + operation
                  + "\\s*");
        String firstOperande = operandes[0].replaceAll("\\s*=\\s*", "");
        String secondOperande = operandes[1];
        double[] firstDouble = new double[nblines];
        double[] secondDouble = new double[nblines];
        final  String[] valueDouble = new String[nblines];
        //get operande data
        int indexColumn1 = getIndexColumn(firstOperande);
        int indexColumn2 = getIndexColumn(secondOperande);
        System.out.println(indexColumn1);
        System.out.println(indexColumn2);
        String[] firstOpKey = new String[nblines];
        for (int k = 0; k < nblines; k++) {
          firstOpKey[k] = data[k][indexColumn1];
        }
        String[] secondOpKey = new String[data.length];
        for (int k = 0; k < nblines; k++) {
          secondOpKey[k] = data[k][indexColumn2];
        }
        //is numeric
        try {
          for (int k = 0; k < firstOpKey.length; k++) {
            firstDouble[k] = Double.parseDouble(firstOpKey[k]);
          }
        } catch (NumberFormatException e) {
          System.out.println("Attribute " + firstOperande
                           + " has no numerical value, "
                  + "please check your config file!");

        }

        try {
          for (int k = 0; k < data.length; k++) {
            secondDouble[k] = Double.parseDouble(secondOpKey[k]);
          }
        } catch (NumberFormatException e) {
          System.out.println("Attribute " + secondOperande
                           + " has no numerical value, "
                  + "please check your config file!");

        }
        //effectuer les operation
        for (int k = 0; k < valueDouble.length; k++) {
          valueDouble[k] = handelOperation(firstDouble[k],
            secondDouble[k], operation);
        }
        newColumnsOperations.add(valueDouble);
      }
    }
    return newColumnsOperations;
  }

  /**
   *a helper methode to parse windows ini config files.
   * @return The list of parsed Ini file sections.
  */

  public static List<String> parseConfigIniFileOperations() {
    if (!cp.configFileExists()) {
      cp.createConfigIni();
    }
    List<String> operations = new ArrayList<String>();
    String path = "config.ini";
    Wini ini;
    try {
      ini = new Wini(new File(path));
      for (String name : ini.get("OperationsCSV").keySet()) {
        String operation = name + " = " + ini.get("OperationsCSV", name);
        operations.add(operation);
      }
    } catch (InvalidFileFormatException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return operations;
  }

  /**
   * method that get index of column.
   * @param columnName name of column hedaer.
   * @return index of column name.
   * @throws CsvValidationException Not valid csv exception.
   * @throws FileNotFoundException  File not found exception.
   * @throws IOException            an In/Out exception.
   */

  public int getIndexColumn(final String columnName)
      throws CsvValidationException, FileNotFoundException,
          IOException {
    String[] headers = getHeadersCsv();
    for (int i = 0; i < headers.length; i++) {
      if (headers[i].equals(columnName)) {
        return i;
      }
    }
    return -1;
  }

  /**
 * Convert csv file to json.
 *
 * @throws IOException            an In/Out exception
 * @throws FileNotFoundException  File not found exception
 * @throws CsvValidationException Not valid csv exception
 * @throws CsvException           all exceptions for opencsv
 */
  public void convertToJson() throws IOException,
      CsvValidationException, CsvException {
    // path of jsonFile
    String jsonFileName = getJsonPath();
    // create new empty json file
    try (OutputStream jsonFile = new FileOutputStream(jsonFileName)) {
      // get headers csv
      String[] headers = getHeadersCsv();
      // get file data
      String[][] data = getAllData(getCsvPath());
      //get columns index of selected attributes
      int[] indexColumns = getIndexColumns();
      StringBuilder jsonData = new StringBuilder();
      // write jsonfile
      jsonData.append("[\n");
      for (int i = 0; i < data.length; i++) {
        jsonData.append("  {\n");
        for (int j = 0; j < indexColumns.length; j++) {
          if (j != indexColumns.length - 1) {
            jsonData.append("    \"" + headers[indexColumns[j]]
                + "\"" + ":" + "\"" + data[i][indexColumns[j]] + "\"" + ",\n");
          } else {
            jsonData.append("    \"" + headers[indexColumns[j]]
                  + "\"" + ":" + "\"" + data[i][indexColumns[j]] + "\"" + "\n");
          }
        }
        if (i != data.length - 1) {
          jsonData.append("  },\n");
        } else {
          jsonData.append("  }\n");
        }
      }
      jsonData.append("]");
      byte[] jsonBytes = jsonData.toString().getBytes(Charset.forName("UTF-8"));
      jsonFile.write(jsonBytes);
      jsonFile.close();

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }
}
