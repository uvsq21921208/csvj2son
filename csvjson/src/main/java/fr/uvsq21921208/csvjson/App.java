
package fr.uvsq21921208.csvjson;

import java.io.IOException;


/**
 * Main application class.
 * @author Mouttie Djekhaba and Sarra Belmahdi.
 *
 */
public final class App {
  /**
   * private constructor.
   */
  private App() {
  }

  /**
  * Main method.
  * @param args command line args
  * @throws IOException an In/Out Exception
  * @throws Exception a standard Exception
  */
  public static void main(final String[] args) throws IOException, Exception {
    String filePath = args[0];
    String extension = filePath.toString().substring(filePath.toString()
        .lastIndexOf("."),
        filePath.length());
    if (extension.equals(".json")) {
      JsonToCsv.convertJsonToCsv(filePath);
    } else {
      if (extension.equals(".csv")) {
        CsvToJson csvtojson = new CsvToJson(filePath);
        csvtojson.convertToJson();
      } else {
        System.out.println("undefind extension");
      }
    }
  }
}
