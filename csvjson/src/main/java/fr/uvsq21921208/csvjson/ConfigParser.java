package fr.uvsq21921208.csvjson;

import java.io.File;
import java.io.IOException;

import org.ini4j.Wini;
/**
 * A class to check and create ini file.
 *
 * @author Mouttie Djekhaba and Sarra Belmahdi.
 *
 */

public class ConfigParser {


  /**
  * helper method to check if the ini file exists.
  * @return True if the ini file exists else returns false.
  */
  public boolean configFileExists() {
    File iniFile = new File("config.ini");
    return (iniFile.exists() && !iniFile.isDirectory());
  }

  /**
  * helper method to create config file (if not exist).
  */
  public void createConfigIni() {
    File iniFile = new File("config.ini");
    try {
      boolean result = iniFile.createNewFile();
      Wini ini = new Wini(new File(iniFile.getAbsolutePath()));
      ini.add("AttributesJSON");
      ini.add("AttributesJSON", "att", "*");
      ini.add("OperationsJSON");
      ini.add("OperationsJSON", "operation", "*");
      ini.add("AttributesCSV");
      ini.add("AttributesCSV", "att", "*");
      ini.add("OperationsCSV");
      ini.add("OperationsCSV", "operation", "null");
      ini.store();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

}
