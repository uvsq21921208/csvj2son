﻿
# CsvJson Converter

A java utility used to convert a csv file to json file and vice versa.

# Features:

  - Convert any json file to csv file.
  - Convert any csv file to json file.
  - Attributes selection.
  - Attributes addition to output files.

# How to use ?

#### To convert a json file to csv, type the following command in jar directory:            

    java -jar csvjson.jar PathToJsonFile.json

this will create a csv file name output.csv located in the same directory as the jar.

#### To convert a csv file to json, type the following command in jar directory:                 
            

    java -jar csvjson.jar PathToCsvFile.csv

this will create a json file name output.csv located in the same directory as the jar.

Additionnaly, you can edit the "config.ini" file to set your own rules.
#### How to write a proper rule in config.ini ?
[AttributesJSON] section represents the json attributes that should be kept in the output csv file.
To select attributes, eddit the attributes option in the config file:


```javascript
[AttributesJSON]
attributes = price,tva,size
```
replacing the value of attributes with a * will keep all attributes

```javascript
[AttributesJSON]
attributes = *
```

[OperationsJSON] section represents the operations that should be applied on the generated csv file.
To define an operation on a json input file:
Under [OperationJSON] section, 
```javascript
[OperationJSON]
Target_attribute  = attribute_one "operation" attribute_two
```
where operation is : [+/*-] (one of them).

#### Example:

```javascript
    [AttributesJSON]
    att = price,size,tva
    [OperationsJSON]
    PriceTVA = TVA * price
```

for example, in our original json we did not have an attribute named priceTVA, this rule : ```PriceTVA = TVA * price ```
will create a new attribute in the generated csv file and its value is evaluated according to the operation.
Given the following json file:
```javascript
{ 
    "Fruit": "Apple",
    "Price": "6",
    "TVA": "0.3"
}
```
After running the jar file with this json file as an iput, it will generate the following csv:

|     Fruit     |       Price     | TVA  | PriceTVA
| :-------------: |:-------------:| :-----:|:-----:|
| Apple   | 6| 0.3 |1.8 

as you can see, the PriceTVA column(attribute) does not exist in the source file and was created thanks to the rule written in our ```config.ini``` file.

# Built with
-  [Maven](https://maven.apache.org/) - Dependency Management

# Authors

- ****Mouttie Djekhaba.****
- ****Sarra Belmahdi.****
